package com.example.appxuts

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.SimpleAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_admin_dashboard.*

class AdminDashboard : AppCompatActivity() , View.OnClickListener {

    val COLLECTION = "Anggota"
    val Kode = "NIK"
    val Nama = "Nama Anggota"
    val Jenis = "Jenis Kelamin"
    val Alamat = "Alamat"
    var docId = ""
    lateinit var db: FirebaseFirestore
    lateinit var alAnggota: ArrayList<HashMap<String, Any>>
    lateinit var adapter: SimpleAdapter
    var fbAuth = FirebaseAuth.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_admin_dashboard)
        btLogoff.setOnClickListener {
            fbAuth.signOut()
            startActivity(Intent(this, Login::class.java))
        }
        alAnggota = ArrayList()
        btnInsert.setOnClickListener(this)
        btnUpdate.setOnClickListener(this)
        btnDelete.setOnClickListener(this)
        btPesanan.setOnClickListener(this)
        lsData.setOnItemClickListener(itemClick)

    }

    override fun onStart() {
        super.onStart()
        db = FirebaseFirestore.getInstance()
        db.collection(COLLECTION).addSnapshotListener { querySnapshot, e ->
            if (e != null) Log.d("firestore", e.message)
            showData()
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnInsert -> {
                val hm = HashMap<String, Any>()
                hm.set(Kode, edKode.text.toString())
                hm.set(Nama, edNamaproduk.text.toString())
                hm.set(Jenis, edJenis.text.toString())
                hm.set(Alamat, edHarga.text.toString())
                db.collection(COLLECTION).document(edKode.text.toString()).set(hm)
                    .addOnSuccessListener {
                        Toast.makeText(this, "Data Sukses Ditambahkan", Toast.LENGTH_SHORT)
                            .show()
                    }.addOnFailureListener { e ->
                        Toast.makeText(
                            this,
                            "Data Gagal Ditambahkan : ${e.message}", Toast.LENGTH_SHORT
                        )
                            .show()
                    }
            }
            R.id.btnUpdate ->{
                val hm = HashMap<String, Any>()
                hm.set(Kode,docId)
                hm.set(Nama,edNamaproduk.text.toString())
                hm.set(Jenis,edJenis.text.toString())
                hm.set(Alamat,edHarga.text.toString())
                db.collection(COLLECTION).document(docId).update(hm)
                    .addOnSuccessListener {
                        Toast.makeText(this, "Data Sukses Diedit",Toast.LENGTH_SHORT)
                            .show() }
                    .addOnFailureListener { e ->
                        Toast.makeText(this,
                            "Data Gagal Diedit : ${e.message}",Toast.LENGTH_SHORT)
                            .show()
                    }
            }
            R.id.btnDelete ->{
                db.collection(COLLECTION).whereEqualTo(Kode,docId).get().addOnSuccessListener {
                        results ->
                    for (doc in results){
                        db.collection(COLLECTION).document(doc.id).delete()
                            .addOnSuccessListener {
                                Toast.makeText(this, "Data sukses dihapus",
                                    Toast.LENGTH_SHORT).show()
                            }.addOnFailureListener { e ->
                                Toast.makeText(this, "Data gagal dihapus ${e.message}",
                                    Toast.LENGTH_SHORT).show()
                            }
                    }
                }.addOnFailureListener { e ->
                    Toast.makeText(this, "Data gagal dihapus ${e.message}",
                        Toast.LENGTH_SHORT).show()
                }
            }

            R.id.btPesanan ->{
                val intent = Intent(this,Pesanan::class.java)
                startActivity(intent)
            }
        }
    }

    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val hm = alAnggota.get(position)
        docId = hm.get(Kode).toString()
        edKode.setText(docId)
        edNamaproduk.setText(hm.get(Nama).toString())
        edJenis.setText(hm.get(Jenis).toString())
        edHarga.setText(hm.get(Alamat).toString())
    }

    fun showData(){
        db.collection(COLLECTION).get().addOnSuccessListener { result ->
            alAnggota.clear()
            for (doc in result){
                val hm = HashMap<String,Any>()
                hm.set(Kode,doc.get(Kode).toString())
                hm.set(Nama,doc.get(Nama).toString())
                hm.set(Jenis,doc.get(Jenis).toString())
                hm.set(Alamat,doc.get(Alamat).toString())
                alAnggota.add(hm)
            }
            adapter = SimpleAdapter(this,alAnggota,R.layout.row_data,
                arrayOf(Kode,Nama,Jenis,Alamat),
                intArrayOf(R.id.txKp,R.id.txNamaproduk,R.id.txJenis,R.id.txHarga))
            lsData.adapter = adapter
        }
    }
        }
    }
}